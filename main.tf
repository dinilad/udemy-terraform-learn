provider "aws"{
    region = "us-east-2"
    access_key = "AKIA4WDJOXJFENQARJMJ"
    secret_key = "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"

}
variable "subnet1_cidr_block" {
  description = "subnet cidr block"
}

variable "cidr_block" {
  description = "cidr block"
  type= list
}
variable "environment" {
  description = "deployment environment"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = var.cidr_block[0]
        vpc-env = "Dev"
    }  
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.cidr_block[1]
  availability_zone = "us-east-2a"
  tags = {
        Name: "dev-subnet-1"
    } 
}

data "aws_vpc" "existing_vpc" {
    default = true  
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = var.cidr_block[2]
    availability_zone = "us-east-2a" 
    tags = {
        Name: "dev-subnet-2"
    }  
}

output "dev-vpc-id" {
    value = aws_vpc.development-vpc.id

}


output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
    
}